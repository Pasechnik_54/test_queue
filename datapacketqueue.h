
#include "genericqueue.h"
#include "datapacket.h"
#include "ipacketqueue.h"

using namespace MyContainer;

template  <int MAX_ONE_PACK_COUNT, int MAX_QUEUE_COUNT, int MAX_QUEUE_SIZE>

class DataPacketQueue : public IPacketQueue
{
public:
    DataPacketQueue();
    ~DataPacketQueue();

    bool Push(unsigned char* data, unsigned int dataCount, unsigned int packetId) override;

    bool Pop(unsigned char* buffer, unsigned int bufferSize, unsigned int& dataCount, unsigned int& packetId) override;

private:
    GenericQueue<DataPacket, MAX_QUEUE_COUNT>* p_PacketQueue = nullptr;

    int _currQueueSize = 0;

    bool sizeQueueInBytesExceedLimit(int sizeAddedPacket);
};



template  <int MAX_ONE_PACK_COUNT, int MAX_QUEUE_COUNT, int MAX_QUEUE_SIZE>
DataPacketQueue<MAX_ONE_PACK_COUNT, MAX_QUEUE_COUNT, MAX_QUEUE_SIZE>::DataPacketQueue()
{
    p_PacketQueue = new GenericQueue<DataPacket, MAX_QUEUE_COUNT>();
}

template  <int MAX_ONE_PACK_COUNT, int MAX_QUEUE_COUNT, int MAX_QUEUE_SIZE>
DataPacketQueue<MAX_ONE_PACK_COUNT, MAX_QUEUE_COUNT, MAX_QUEUE_SIZE>::~DataPacketQueue()
{
    delete p_PacketQueue;
    p_PacketQueue = nullptr;
}



template  <int MAX_ONE_PACK_COUNT, int MAX_QUEUE_COUNT, int MAX_QUEUE_SIZE>

// Поместить пакет в очередь
// data - содержимое пакета
// dataCount - длина пакета в байтах
// packetId - идентификатор пакета
// return - результат постановки пакета в очередь:
//   true - пакет помещен в очередь
//   false - длина пакета превышает максимально допустимую, число пакетов в очереди или суммарный размер пакетов в очереди превысили максимально допустимые

bool DataPacketQueue<MAX_ONE_PACK_COUNT, MAX_QUEUE_COUNT, MAX_QUEUE_SIZE>::Push(unsigned char* data,
                                                                                unsigned int dataCount, unsigned int packetId)
{    
    if (dataCount > MAX_ONE_PACK_COUNT)
    {
        return false;
    }
    if ( sizeQueueInBytesExceedLimit( dataCount * sizeof(data[0]) ) )
    {
        return false;
    }
    if ( (p_PacketQueue->Count() + 1) > MAX_QUEUE_COUNT)       
    {
        return false;
    }   

    DataPacket pack(data, dataCount, packetId);
    p_PacketQueue->Push(pack);
    _currQueueSize += pack.packCount * sizeof(data[0]);

    return true;
}

template  <int MAX_ONE_PACK_COUNT, int MAX_QUEUE_COUNT, int MAX_QUEUE_SIZE>

// Извлечь пакет из очереди
// buffer - приемный буфер, куда помещаются данные извлеченные из очереди
// bufferSize - размер приемного буфера
// dataCount - фактический размер пакета, извлеченного из очереди
// packetId - идентификатор пакета, извлеченного из очереди
// return - результат извлечения пакета из очереди:
//   true - пакет извлечен и помещен в буфер (в том числе, если размер пакета превысил размер приемного буфера)
//   false - очередьпуста

bool DataPacketQueue<MAX_ONE_PACK_COUNT, MAX_QUEUE_COUNT, MAX_QUEUE_SIZE>::Pop(unsigned char* buffer, unsigned int bufferSize,
                                                                               unsigned int& dataCount, unsigned int& packetId)
{
    
    if ( p_PacketQueue->IsEmpty() )
    {
        return false;
    }
    DataPacket pack;
    bool resultOk = p_PacketQueue->Pop(pack);
    if (!resultOk)
    {
        return false;
    }
    if (pack.p_payload == nullptr) // если по какой-то причине в данных пакета окажется nullptr
    {
        std::cout << "Warning: no data in packet" << std::endl;
        return false;
    }

    packetId = pack.id;
    dataCount = pack.packCount;

    if (bufferSize >= pack.packCount)
    {
        memcpy(buffer, pack.p_payload, pack.packCount);
    }
    else
    {
        memcpy(buffer, pack.p_payload, bufferSize);
    }
    
    _currQueueSize -= pack.packCount * sizeof(pack.p_payload[0]);
    
    return true;
}


template  <int MAX_ONE_PACK_COUNT, int MAX_QUEUE_COUNT, int MAX_QUEUE_SIZE>

bool DataPacketQueue<MAX_ONE_PACK_COUNT, MAX_QUEUE_COUNT, MAX_QUEUE_SIZE>::sizeQueueInBytesExceedLimit(int sizeAddedPacket)
{
    
    if (sizeAddedPacket + _currQueueSize > MAX_QUEUE_SIZE)
    {
        return true;
    }
    else
    {
        return false;
    }
    
}

