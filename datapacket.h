
struct DataPacket
{
public:

    unsigned int id = 0;
    unsigned char* p_payload = nullptr;
    unsigned int packCount = 0;

    DataPacket(unsigned char* data, unsigned int dataCount, unsigned int packId)
    {
        InitDataPacket(data, dataCount, packId);
    }    

    DataPacket(DataPacket &dataPacket) // ����������� ����������� (�.�. ��������� �������� ���������)
    {
        copyDataPacket(dataPacket);
    }

    DataPacket& operator=(const DataPacket& dataPacket)
    {
        deleteDataPacket();
        copyDataPacket(dataPacket);

        return *this;
    }

    DataPacket()
    {
        id = 0;
        packCount = 0;
        p_payload = nullptr;
    }
    
    ~DataPacket()
    {        
        deleteDataPacket();        
    }

private:
    void deleteDataPacket()
    {
        id = 0;
        packCount = 0;
        delete[] p_payload;
        p_payload = nullptr;
    }

    void copyDataPacket(const DataPacket &srcPacket)
    {
        InitDataPacket(srcPacket.p_payload, srcPacket.packCount, srcPacket.id);
    }

    void InitDataPacket(unsigned char* data, unsigned int dataCount, unsigned int packId)
    {
        id = packId;
        packCount = dataCount;
        p_payload = nullptr;
        if (packCount > 0)
        {
            p_payload = new unsigned char[packCount];
            memcpy(p_payload, data, packCount);
        }        
        
        // ���� ����� ������ 0, ������� ������ ����-��������������� ������, ����� �������� ���������� ��������� �� nullptr
        // �������� ������� ����� packCount ������� ������ 1, �� �� ����, ���� �������� �������� �� ���� ���
        // � ����� �������� ����� �� ����, ����� ��������� ���������� ��� ����� ������� ������ = 0, ���� ��� ��������� ����� c-������
        else
        {
            const int EMPTY_NULL_TERMINATE_STRING_LENGTH = 1;
            p_payload = new unsigned char[EMPTY_NULL_TERMINATE_STRING_LENGTH];
            p_payload[0] = 0; 
            //packCount = EMPTY_NULL_TERMINATE_STRING_LENGTH;
        }
        
    }

};

