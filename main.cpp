
#include <iostream>
#include <fstream>
#include <string>

#include "datapacketqueue.h"

using namespace std;

enum class TestType
{
    ReadFullFile,
    ExceedMaxOnePackCount,
    ExceedMaxQueueCount,
    ExceedMaxQueueSize
};
// ������� ��� �������� �������� ������ � ������ � ���� ����� �������
bool createGenericTestTxtFile(const char* path);
bool createByteSizeTestTextFile(const char* path);
void runTest(const char* path, TestType testType);


int main()
{
    // ����� ������� � �������� �������
    const char INPUT_GEN_FILE_NAME[] = "genericInputData.txt";
    const char INPUT_BYTE_SIZE_FILE_NAME[] = "byteSizeInputData.txt";

    int errorCode = 1;

    bool testGenFileCreated = createGenericTestTxtFile(INPUT_GEN_FILE_NAME);
    if (!testGenFileCreated)
    {
        return errorCode;
    }

    bool testByteSizeFileCreated = createByteSizeTestTextFile(INPUT_BYTE_SIZE_FILE_NAME);
    if (!testByteSizeFileCreated)
    {
        return errorCode;
    }

    runTest(INPUT_GEN_FILE_NAME, TestType::ReadFullFile);
    runTest(INPUT_GEN_FILE_NAME, TestType::ExceedMaxOnePackCount);
    runTest(INPUT_GEN_FILE_NAME, TestType::ExceedMaxQueueCount);
    runTest(INPUT_BYTE_SIZE_FILE_NAME, TestType::ExceedMaxQueueSize);

    
    /*
    DataPacketQueue<10, 16000, 4096> dataQueue;// = DataPacketQueue<10, 20, 1000>();

    ifstream inputFile("D:\\inputData.txt"); // �������� ���� ��� ������

    if (inputFile.is_open())
    {
        cout << "input file open" << endl;

        string str;        
        unsigned int packetId = 1;

        while (getline(inputFile, str))
        {
            
            bool resultOk = dataQueue.Push( (unsigned char*)str.c_str(), str.size()+1, packetId);
            if (resultOk)
            {
                packetId++;
            }
            else
            {
                cout << "stop queue push" << endl;
                break;
            }
        }

        cout << "reading is ended, packets count = " << packetId << endl;

        inputFile.close();
        cout << "input file closed" << endl;
    }
    else
    {
        cout << "file not found" << endl;
    }



    ofstream outFile;
    outFile.open("D:\\outData.txt"); 

    if (outFile.is_open())
    {
        cout << "output file open" << endl;
                
        const unsigned int BUFFER_SIZE = 200;
        unsigned char dataBuffer[BUFFER_SIZE];

        bool resultOk = false;
        do {

            unsigned int packetId = 0;
            unsigned int dataCount = 0;

            resultOk = dataQueue.Pop(&dataBuffer[0], BUFFER_SIZE, dataCount, packetId);
                        
            if (resultOk)
            {                   
                //string str(reinterpret_cast<char const*>(dataBuffer), dataCount );
                string str;
                auto c_str = str.c_str();
                c_str = (char*)dataBuffer;
                cout << "getted packet successfully" << endl;
                //outFile << str << endl;
                outFile << c_str << endl;

                cout << "data count = " << dataCount << " packetId = " << packetId << endl;
            }
            else
            {
                cout << "getted packet fault" << endl;
            }

        } while (resultOk);
        
        outFile.close();
        cout << "output file closed" << endl;
    }
    else
    {
        cout << "error open out file" << endl;
    }

    */

    return 0;
}

bool createGenericTestTxtFile(const char* path)
{
    ofstream createdFile;
    createdFile.open(path);

    if ( createdFile.is_open() )
    {
        cout << "start creating generic test txt file" << endl;

        char str1[] = "a\n";
        char str2[] = "bb\n";
        char str3[] = "ccc\n";
        char str4[] = "dddd\n";
        char str5[] = "eeeee\n";
        char str6[] = "ffffff\n";        
        
        int cyleCount = 80;
        for (int i = 0; i < cyleCount; i++)
        {
            createdFile << str1 << str2 << str3 << str4 << str5 << str6;
        }

        createdFile.close();
        cout << "created test generic txt file complete" << endl;
        return true;
    }
    else
    {
        cout << "error create generic input file" << endl;
        return false;
    }
}

bool createByteSizeTestTextFile(const char* path)
{
    ofstream createdFile;
    createdFile.open(path);

    if (createdFile.is_open())
    {
        cout << "start creating byte size test txt file" << endl;

        char str[] = "ggggggg\n";

        int cyleCount = 200;
        for (int i = 0; i < cyleCount; i++)
        {            
            createdFile << str;
        }

        createdFile.close();
        cout << "created byte size test txt file complete" << endl;
        return true;
    }
    else
    {
        cout << "error create byte size input file" << endl;
        return false;
    }
}

void runTest(const char* pathInputFile, TestType testType)
{
    
    IPacketQueue *p_packQueue = nullptr;
    string outFilename;    
    
    const int TEST_MAX_COUNT_ONE_STR = 4; // ���� ���������� �������� � ����������� ������ (������� ������������)
    const int TEST_MAX_COUNT_FOR_QUEUE = 100; // ���� ���������� ����� ��� ������ � �������
    const int TEST_MAX_SIZE_FOR_QUEUE = 1024; // ������������ ������ �������    

    switch (testType)
    {
    case TestType::ReadFullFile:
        // ����� ���������� �������� ��������� ���� ��������������� ����        
        p_packQueue = new DataPacketQueue<10, 512, 4096>();
        outFilename = "testReadFull.txt";
        break;
    case TestType::ExceedMaxOnePackCount:
        p_packQueue = new DataPacketQueue<TEST_MAX_COUNT_ONE_STR, 512, 4096>();
        outFilename = "testExceedMaxOnePackCount.txt";
        break;
    case TestType::ExceedMaxQueueCount:
        p_packQueue = new DataPacketQueue<10, TEST_MAX_COUNT_FOR_QUEUE, 4096>();
        outFilename = "testExceedMaxQueueCountOut.txt";
        break;
    case TestType::ExceedMaxQueueSize:        
        p_packQueue = new DataPacketQueue<10, 512, TEST_MAX_SIZE_FOR_QUEUE>();
        outFilename = "testExceedMaxQueueSizeOut.txt";
        break;
    
    }

    cout << "----------------- start test: " << outFilename << endl;
    
    // ��������� � �������
    ifstream inputFile;
    inputFile.open(pathInputFile);

    if (inputFile.is_open())
    {
        cout << "input file open" << endl;

        string str;
        unsigned int packetId = 0;

        while (getline(inputFile, str))
        {
            packetId++;

            bool resultOk = p_packQueue->Push((unsigned char*)str.c_str(), str.size() + 1, packetId);
            if (resultOk)
            {
                cout << "packet pushed ok, id = " << packetId << " data length = " << str.size() + 1 << endl;
            }
            else
            {
                cout << "packet not pushed, id = " << packetId << endl;                
            }            
            
        }
        cout << "reading is ended, packets count = " << packetId << endl;

        inputFile.close();
        cout << "input file closed" << endl;

    }
    else
    {
        cout << "file not found" << endl;
    }
    

    // ��������� �� �������    
    ofstream outFile;
    outFile.open(outFilename);

    if (outFile.is_open())
    {
        cout << "output file open" << endl;

        const unsigned int BUFFER_SIZE = 200;
        unsigned char dataBuffer[BUFFER_SIZE];

        bool resultOk = false;
        do {

            unsigned int packetId = 0;
            unsigned int dataCount = 0;

            resultOk = p_packQueue->Pop(&dataBuffer[0], BUFFER_SIZE, dataCount, packetId);

            if (resultOk)
            {                
                char* str = (char*)dataBuffer;
                
                outFile << str << endl;

                //cout << "data count = " << dataCount << " packetId = " << packetId << endl;
            }
            else
            {
                cout << "queue empty" << endl;
                break;
            }

        } while (resultOk);

        outFile.close();
        cout << "output file closed" << endl;
    }
    else
    {
        cout << "error open out file" << endl;
    }
    
    delete p_packQueue;
    p_packQueue = nullptr;    

    cout << "----------------- end test: " << outFilename << endl;
    cout << endl;
}


