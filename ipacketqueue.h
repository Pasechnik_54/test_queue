

class IPacketQueue
{

public:
	virtual ~IPacketQueue() {}

	virtual bool Push(unsigned char* data, unsigned int dataCount, unsigned int packetId) = 0;
	virtual bool Pop(unsigned char* buffer, unsigned int bufferSize, unsigned int& dataCount, unsigned int& packetId) = 0;
};