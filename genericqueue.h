#include <stdio.h>

namespace MyContainer
{

template <typename T, int MAX_QUEUE_COUNT>
class GenericQueue
{

public:
	GenericQueue();
	~GenericQueue();

	bool Push(const T &data);
	bool Pop(T &data);

	int Count() { return _count; }
	bool IsEmpty() { return _end == _start; }

private:
	T *p_queueArray = nullptr;

	int _count = 0;
	int _start = 0;
	int _end = 0;	

	void shiftIndex(int& index);
};

template <typename T, int MAX_QUEUE_COUNT>
GenericQueue<T, MAX_QUEUE_COUNT>::GenericQueue()
{
	p_queueArray = new T[MAX_QUEUE_COUNT + 1];
}

template <typename T, int MAX_QUEUE_COUNT>
GenericQueue<T, MAX_QUEUE_COUNT>::~GenericQueue()
{
	delete[] p_queueArray;
	p_queueArray = nullptr;
}


template <typename T, int MAX_QUEUE_COUNT>
bool GenericQueue<T, MAX_QUEUE_COUNT>::Push(const T &data)
{
	if (Count() == MAX_QUEUE_COUNT)
	{
		return false;
	}

	p_queueArray[_end] = data;
	_count++;
	shiftIndex(_end);

	return true;
}

template <typename T, int MAX_QUEUE_COUNT>
bool GenericQueue<T, MAX_QUEUE_COUNT>::Pop(T &data)
{
	if ( IsEmpty() ) // ���� ������� ������, �������
	{
		return false;
	}

	data = p_queueArray[_start];
	_count--;
	shiftIndex(_start);	

	return true;
}

template <typename T, int MAX_QUEUE_COUNT>
void  GenericQueue<T, MAX_QUEUE_COUNT>::shiftIndex(int& index)
{	
	// ���������� � ������ �������, ����� ������� �� �����
	if (index == MAX_QUEUE_COUNT)
	{
		index = 0;
	}
	else
	{
		index++;
	}
}


}
